import numpy as np

import os
from abc import ABC, abstractmethod
from PIL import Image, ImageDraw


class GridWorld:
    def __init__(self, *, board, state=(0, 0)):
        self.board = board
        self.state = state

    def reward(self):
        value = self.board[self.state]
        if value == 2:
            return 1
        if value == 3:
            return -1
        return 0

    def nextState(self, action):
        # 0 = up, 1 = right, 2 = down, 3 = left
        if action == 0:
            nextState = (self.state[0], self.state[1] - 1)
        elif action == 1:
            nextState = (self.state[0] + 1, self.state[1])
        elif action == 2:
            nextState = (self.state[0], self.state[1] + 1)
        elif action == 3:
            nextState = (self.state[0] - 1, self.state[1])
        else:
            raise ValueError(f"Invalid action {action}")

        # Legal move?
        if \
                nextState[0] < 0 or \
                nextState[0] >= len(self.board) or \
                nextState[1] < 0 or \
                nextState[1] >= len(self.board[0]) or \
                self.board[nextState] == 1:
            nextState = self.state
        return nextState
    
    def reset(self):
        self.state = (0, 0)

class BaseAgent(ABC):
    def __init__(self, env, seed=42):
        self.env = env
        self.rng = np.random.default_rng(seed)
    
    @abstractmethod
    def action(self):
        pass

class RandomAgent(BaseAgent):
    def action(self):
        return self.rng.integers(0, 4)

class QLearningAgent(BaseAgent):
    def __init__(self, *args, **kwargs):
        super(QLearningAgent, self).__init__(*args, **kwargs)
        self.learningRate = 0.1
        self.explorationRate = 0.2
        self.Q = np.zeros((self.env.board.shape[0], self.env.board.shape[1], 4))
        self.decayRate = 0.99
        self.states = []

    def reset(self):
        self.states = []
        self.env.reset()

    def action(self):
        
        # Explore
        if self.rng.uniform() <= self.explorationRate:
            return self.rng.integers(0, 4)
        
        # Exploit
        bestNextReward = -np.inf
        bestNextAction = None

        for action in range(4):
            nextReward = self.Q[self.env.state][action]
            if nextReward >= bestNextReward:
                bestNextReward = nextReward
                bestNextAction = action
        return bestNextAction
    
    def play(self, rounds=20):
        i = 0
        while i < rounds:
            reward = self.env.reward()
            if reward != 0:
                for action in range(4):
                    self.Q[self.env.state][action] = reward
                # Back prop
                for state, action in reversed(self.states):
                    currentQ = self.Q[state][action]
                    reward = currentQ + self.learningRate * (self.decayRate * reward - currentQ)
                    self.Q[state][action] = round(reward, 3)
                self.reset()
                i += 1
            
            action = self.action()
            self.states.append((self.env.state, action))
            self.env.state = self.env.nextState(action)

def do_single(board, n):
    env = GridWorld(board=board)
    agent = QLearningAgent(env)
    agent.play(n)
    return agent.Q

def draw_board(board):
    board = board.swapaxes(0, 1)[::-1]
    im = Image.new('RGB', (board.shape[1] * 100, board.shape[0] * 100), color='white')
    d = ImageDraw.Draw(im)
    for x in range(board.shape[1]):
        for y in range(board.shape[0]):
            if board[y][x] == 1:
                d.rectangle((x * 100, y * 100, x * 100 + 100, y * 100 + 100), fill='black')
            elif board[y][x] == 2:
                d.rectangle((x * 100, y * 100, x * 100 + 100, y * 100 + 100), fill='green')
            elif board[y][x] == 3:
                d.rectangle((x * 100, y * 100, x * 100 + 100, y * 100 + 100), fill='red')
    im.save('out/board.png')

def draw_q(q, n):
    Q = q[:, ::-1]
    im = Image.new('RGB', (Q.shape[0]*200, Q.shape[1]*200), (255, 255, 255))
    d = ImageDraw.Draw(im)
    for i in range(len(Q)):
        for j in range(len(Q[0])):
            d.text((20 + i * 200 + 50, 20 + j * 200), f'{Q[i][j][0]}', fill=(0, 0, 0), anchor='mt') # up
            d.text((20 + i * 200 + 100, 20 + j * 200 + 50), f'{Q[i][j][1]}', fill=(0, 0, 0), anchor='rs')
            d.text((20 + i * 200 + 50, 20 +  j * 200 + 100), f'{Q[i][j][2]}', fill=(0, 0, 0), anchor='mb')
            d.text((20 + i * 200,20 +  j * 200 + 50), f'{Q[i][j][3]}', fill=(0, 0, 0), anchor='ls')
            direction = ['up', 'right', 'down', 'left'][np.argmax(Q[i][j])]
            d.text((20 + i * 200 + 50, 20 + j * 200 + 50), f'{direction}', fill=(0, 0, 0), anchor='center')
    im.save(f'out/Q_{n}.png')

def main():
    board = np.array([
        # 0 = empty, 1 = wall, 2 = win, 3 = loss
        [0, 0, 0, 2],
        [0, 1, 0, 3],
        [0, 0, 0, 0],
    ])

    if not os.path.exists('out'):
        os.makedirs('out')

    # mutate board so that (0, 0) is bottom left
    board = board[::-1].swapaxes(0, 1)
    draw_board(board)

    for n in (10, 100, 1000):
        q = do_single(board, n)
        draw_q(q, n)


if __name__ == '__main__':
    main()
